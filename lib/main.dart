import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'src/core/constants/hex_color_code.dart';
import 'src/core/constants/strings.dart';
import 'src/core/di/dependency_injection.dart';
import 'src/core/routes/routes.dart';
import 'src/core/utility/color_tools.dart';

Future<void> main() async{
  WidgetsFlutterBinding.ensureInitialized();
  setup();
  runApp(
    const ProviderScope(
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);
    SystemChrome.setSystemUIOverlayStyle(
      const SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
      ),
    );

    return ScreenUtilInit(
        designSize: const Size(414, 896),
        minTextAdapt: true,
        splitScreenMode: true,
        builder: (context , child)  {
          return MaterialApp.router(
            routerConfig: goRouter,
            debugShowCheckedModeBanner: false,
            theme: ThemeData(
              dividerColor: Colors.transparent,
              fontFamily: StringData.fontFamilyPoppins,
              scaffoldBackgroundColor: HexColor(HexColorCode.scaffoldBackgroundColor),
            ),
            title: StringData.appNameText,
          );
        }
    );
  }
}
