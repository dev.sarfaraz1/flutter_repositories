class HexColorCode {
  const HexColorCode._();
  //HexColors
  static const String whiteColor = "FFFFFF";
  static const String blackColor = "000000";
  static const String greyColor = "B6B6B6";
  static const String appPrimaryColor = "1F381E";
  static const String appSecondaryColor = "579CAB";
  static const String scaffoldBackgroundColor = "F4F9FF";
  static const String textColor = "373737";
}
