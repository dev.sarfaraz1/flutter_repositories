class StringData {
  const StringData._();

  static const String appNameText = "Flutter Repositories";
  static const String fontFamilyPoppins = 'Poppins';
  static const String fontFamilyRoboto = 'Roboto';
}
