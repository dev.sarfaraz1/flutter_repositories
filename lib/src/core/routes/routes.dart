import 'package:go_router/go_router.dart';

import '../../feature/flutter_repository/presentation/screens/repository_list_screen.dart';
import '../../feature/flutter_repository/presentation/screens/repository_details_screen.dart';
import '../../feature/flutter_repository/data/models/repository_detail_model.dart';
import '../enums/route_name.dart';

final GoRouter goRouter = GoRouter(
  routes: [
    GoRoute(
      path: "/",
      name: RouteName.repositoryList.name,
      builder: (context, state) => const RepositoryListScreen(),
      routes: [
        GoRoute(
          name: RouteName.repositoryDetails.name,
          path: RouteName.repositoryList.name,
          builder: (context, state) => RepositoryDetailsScreen(repositoryDetailsModel: state.extra as RepositoryDetailsModel),
        ),
      ]
    ),
  ],
);