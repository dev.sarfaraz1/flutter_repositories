import 'package:dartz/dartz.dart';

import '../../data/models/repositories_model.dart';

abstract class FlutterRepository{
  Future<Either<Exception, RepositoriesModel>> fetchRepositories(String query, int currentPage, String sort);

}