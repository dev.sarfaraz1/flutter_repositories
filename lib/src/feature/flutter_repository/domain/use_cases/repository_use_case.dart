import 'package:dartz/dartz.dart';

import '../../data/models/repositories_model.dart';
import '../repositories/flutter_repository.dart';

class RepositoryUseCase {
  final FlutterRepository _flutterRepository;
  RepositoryUseCase(this._flutterRepository);

  Future<Either<Exception, RepositoriesModel>> fetchRepositories(String searchQuery, int currentPage, String sort) async {
    final repositories = await _flutterRepository.fetchRepositories(searchQuery, currentPage, sort);
    return repositories;
  }

}
