import 'package:dartz/dartz.dart';

import '../../data/models/repositories_model.dart';
import '../../data/repositories/flutter_repository_imp.dart';
import '../../domain/use_cases/repository_use_case.dart';

class RepositoryService{
  static final RepositoryUseCase _repositoryUseCase = RepositoryUseCase(FlutterRepositoryImp());

  static Future<Either<Exception, RepositoriesModel>> getRepositories(String searchQuery, int currentPage, String sort) async{
    return _repositoryUseCase.fetchRepositories(searchQuery, currentPage, sort);
  }

}