import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';

import '../../../../core/constants/hex_color_code.dart';
import '../../../../core/utility/color_tools.dart';
import '../../../../core/constants/image_assets.dart';
import '../../data/models/repository_detail_model.dart';

class RepositoryDetailsScreen extends StatelessWidget {
  final RepositoryDetailsModel repositoryDetailsModel;

  const RepositoryDetailsScreen(
      {super.key, required this.repositoryDetailsModel});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          repositoryDetailsModel.fullName ?? "",
          style: TextStyle(
              color: HexColor(HexColorCode.textColor),
              fontSize: 16.0.sp,
              fontWeight: FontWeight.w500),
        ),
        leading: IconButton(
          onPressed: () {
            Navigator.of(context, rootNavigator: true).pop();
          },
          icon: Icon(
            Icons.arrow_back,
            color: HexColor(HexColorCode.textColor),
          ),
        ),
        backgroundColor: HexColor(HexColorCode.scaffoldBackgroundColor),
        centerTitle: true,
        elevation: 0.0,
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0.w, vertical: 16.0.h),
        child: CustomScrollView(
          slivers: <Widget>[
            SliverList(
                delegate: SliverChildListDelegate([
              SizedBox(
                height: 5.h,
              ),
              repositoryDetailsModel.owner?.avatarUrl != null
                  ? CachedNetworkImage(
                      height: 200.0.h,
                      width: double.infinity,
                      fit: BoxFit.contain,
                      imageUrl: repositoryDetailsModel.owner!.avatarUrl!,
                      placeholder: (context, url) =>
                          const CupertinoActivityIndicator(),
                      errorWidget: (context, url, error) => Image.asset(
                          ImageAssets.imgEmptyProfile,
                          fit: BoxFit.contain,
                          width: double.maxFinite,
                          height: 200.0.h),
                    )
                  : Image.asset(ImageAssets.imgEmptyProfile,
                      fit: BoxFit.contain,
                      width: double.maxFinite,
                      height: 200.0.h),
              SizedBox(height: 30.0.h),
              repositoryDetailsModel.fullName != null
                  ? Text(
                      repositoryDetailsModel.fullName!,
                      style: TextStyle(
                        color: HexColor(HexColorCode.textColor),
                        fontSize: 18.sp,
                        fontWeight: FontWeight.w600,
                      ),
                    )
                  : Container(),
              SizedBox(height: 10.0.h),
              repositoryDetailsModel.owner?.login != null
                  ? Text(
                      "Owner: ${repositoryDetailsModel.owner!.login!}",
                      style: TextStyle(
                        color: HexColor(HexColorCode.appSecondaryColor),
                        fontSize: 16.sp,
                        fontWeight: FontWeight.w600,
                      ),
                    )
                  : Container(),
              SizedBox(height: 20.0.h),
              repositoryDetailsModel.description != null
                  ? Text(
                      repositoryDetailsModel.description!,
                      style: TextStyle(
                        color: HexColor(HexColorCode.textColor),
                        fontSize: 14.sp,
                        fontWeight: FontWeight.w400,
                      ),
                    )
                  : Container(),
              SizedBox(height: 30.0.h),
              repositoryDetailsModel.updatedAt != null
                  ? Text(
                      "Last Updated: ${DateFormat('MM-dd-yyyy HH:mm:ss').format(DateTime.parse(repositoryDetailsModel.updatedAt!))}",
                      style: TextStyle(
                        color: HexColor(HexColorCode.textColor),
                        fontSize: 12.sp,
                        fontWeight: FontWeight.w600,
                      ),
                    )
                  : Container(),
              SizedBox(height: 10.0.h),
              repositoryDetailsModel.stargazersCount != null
                  ? Text(
                      "Stars: ${repositoryDetailsModel.stargazersCount}",
                      style: TextStyle(
                        color: HexColor(HexColorCode.textColor),
                        fontSize: 12.sp,
                        fontWeight: FontWeight.w600,
                      ),
                    )
                  : Container(),
              SizedBox(
                height: 30.h,
              ),
            ]))
          ],
        ),
      ),
    );
  }
}
