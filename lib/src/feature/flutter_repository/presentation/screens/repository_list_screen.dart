import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:go_router/go_router.dart';

import '../../../../core/constants/hex_color_code.dart';
import '../../../../core/constants/strings.dart';
import '../../../../core/enums/route_name.dart';
import '../../../../core/utility/color_tools.dart';
import '../provider/repository_provider.dart';
import '../widgets/loader/repository_load.dart';
import '../widgets/repository_widget.dart';

class RepositoryListScreen extends ConsumerStatefulWidget {
  const RepositoryListScreen({super.key});

  @override
  RepositoryListScreenState createState() => RepositoryListScreenState();
}

class RepositoryListScreenState extends ConsumerState<RepositoryListScreen> {
  final ScrollController _scrollController = ScrollController();
  String sort = "";
  Timer? _timer;

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_onScroll);
    _timer = Timer.periodic(const Duration(minutes: 30), (Timer timer) {
      _fetchRepositories();
    });
  }

  @override
  void dispose() {
    _scrollController.removeListener(_onScroll);
    _scrollController.dispose();
    _timer?.cancel();
    super.dispose();
  }

  _fetchRepositories(){
    final paginationNotifier = ref.watch(paginationProvider.notifier);
    paginationNotifier.fetchNextPage(sort);
  }

  void _onScroll() {
    if (_scrollController.position.extentAfter < 300) {
      _fetchRepositories();
    }
  }

  _sort(String value) {
    final paginationNotifier = ref.watch(paginationProvider.notifier);
    paginationNotifier.state.clear();
    paginationNotifier.fetchNextPage(sort);
  }

  @override
  Widget build(BuildContext context) {
    final repositoryList = ref.watch(paginationProvider);
    return Scaffold(
      appBar: AppBar(
        title: Text(
          StringData.appNameText,
          style: TextStyle(
            color: HexColor(HexColorCode.textColor),
          ),
        ),
        backgroundColor: HexColor(HexColorCode.scaffoldBackgroundColor),
        actions: [
          Center(
            child: Text(
              sort.toUpperCase(),
              style: TextStyle(
                color: HexColor(HexColorCode.appSecondaryColor),
                fontSize: 12.sp,
              ),
            ),
          ),
          PopupMenuButton(
            icon: Icon(Icons.sort,
                color: HexColor(HexColorCode.appSecondaryColor)),
            itemBuilder: (BuildContext context) {
              return [
                PopupMenuItem(
                    value: "updated",
                    child: Text(
                      "UPDATED",
                      style: TextStyle(
                          fontSize: 11.0,
                          color: HexColor(HexColorCode.textColor)),
                    )),
                PopupMenuItem(
                    value: "stars",
                    child: Text(
                      "STARS",
                      style: TextStyle(
                          fontSize: 11.0,
                          color: HexColor(HexColorCode.textColor)),
                    )),
              ];
            },
            onSelected: (value) {
              if (mounted) {
                setState(() {
                  sort = value;
                });
              }
              _sort(value);
            },
          ),
        ],
        // centerTitle: true,
        elevation: 0.0,
      ),
      body: RefreshIndicator(
        backgroundColor: HexColor(HexColorCode.whiteColor),
        color: HexColor(HexColorCode.appSecondaryColor),
        onRefresh: () async {
          setState(() {
            sort = "";
          });
          ref.refresh(paginationProvider.notifier).fetchNextPage(sort);
        },
        child: CustomScrollView(
          controller: _scrollController,
          slivers: <Widget>[
            SliverList(
              delegate: SliverChildBuilderDelegate(
                (context, index) {
                  if (index < repositoryList.length) {
                    final repository = repositoryList[index];
                    return GestureDetector(
                      onTap: () {
                        context.goNamed(RouteName.repositoryDetails.name,
                            extra: repository);
                      },
                      child: RepositoryWidget(
                        name: repository.fullName ?? "",
                        description: repository.description ?? "",
                        updatedTime: repository.updatedAt ?? "",
                        stars: repository.stargazersCount ?? 0,
                      ),
                    );
                  } else {
                    final paginationNotifier =
                        ref.watch(paginationProvider.notifier);
                    if (paginationNotifier.state.isEmpty) {
                      return RepositoryShimmerWidget(
                          height: 150.0.h, width: double.infinity);
                    } else {
                      return RepositoryShimmerWidget(
                          height: 150.0.h, width: double.infinity);
                    }
                  }
                },
                childCount: repositoryList.length + 1,
              ),
            ),
            SliverList(
                delegate: SliverChildListDelegate([
              SizedBox(
                height: 40.0.h,
              ),
            ]))
          ],
        ),
      ),
    );
  }
}
