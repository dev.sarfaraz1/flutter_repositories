import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../core/utility/log.dart';
import '../../data/models/repository_detail_model.dart';
import '../services/repository_service.dart';

final paginationProvider = StateNotifierProvider.autoDispose<PaginationNotifier,
    List<RepositoryDetailsModel>>((ref) {
  return PaginationNotifier("");
});

class PaginationNotifier extends StateNotifier<List<RepositoryDetailsModel>> {
  int _currentPage = 1;
  bool _isLoading = false;

  PaginationNotifier(String sort) : super([]) {
    fetchNextPage(sort);
  }

  Future<void> fetchNextPage(String sort) async {
    if (_isLoading) return;
    try {
      _isLoading = true;
      if (state.isEmpty) {
        _currentPage = 1;
      }
      final repositories = await RepositoryService.getRepositories(
          "Flutter", _currentPage, sort);

      repositories.fold(
        (l) {
          return state = [];
        },
        (r) {
          state = [...state, ...r.items ?? []];
          _currentPage++;

          return state;
        },
      );
    } catch (e) {
      appPrint('Error: $e');
    } finally {
      _isLoading = false;
    }
  }
}
