import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:readmore/readmore.dart';

import '../../../../core/constants/hex_color_code.dart';
import '../../../../core/utility/color_tools.dart';

class RepositoryWidget extends StatelessWidget{
  final String name;
  final String description;
  final String updatedTime;
  final int stars;
  const RepositoryWidget({super.key, required this.name, required this.description, required this.updatedTime, required this.stars});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 10.0.w, vertical: 2.0.h),
      child: Card(
        elevation: 0.0,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15)),
        child: Column(
          children: <Widget>[
            ListTile(
              title: Text(name,
                style: TextStyle(
                  fontSize: 15.0.sp,
                  fontWeight: FontWeight.w600,
                  color: HexColor(HexColorCode.textColor),
                ),
              ),
              subtitle: Text(DateFormat('MM-dd-yyyy HH:mm:ss').format(DateTime.parse(updatedTime)),
                style: TextStyle(
                  fontSize: 12.0.sp,
                  color: HexColor(HexColorCode.textColor),
                ),
              ),
            ),
            SizedBox(height: 10.0.h,),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 15.0.w),
              child: ReadMoreText(
                description.trim(),
                trimLines: 1,
                style:TextStyle(
                  fontSize: 14.0.sp,
                  fontWeight: FontWeight.w500,
                  color: HexColor(HexColorCode.textColor),
                ),
                textAlign: TextAlign.start,
                colorClickableText: HexColor(HexColorCode.appPrimaryColor),
                trimMode: TrimMode.Line,
                trimCollapsedText: ' Show more',
                trimExpandedText: ' ...Show less',
                moreStyle: TextStyle(
                  fontSize: 12.0.sp,
                  fontWeight: FontWeight.w600,
                  color: HexColor(HexColorCode.appSecondaryColor),
                ),
                lessStyle: TextStyle(
                  fontSize: 12.0.sp,
                  fontWeight: FontWeight.w600,
                  color: HexColor(HexColorCode.appSecondaryColor),
                ),
              )
            ),
            SizedBox(height: 12.0.h,),
            Row(
              children: [
                SizedBox(width: 15.0.w,),
                const Icon(Icons.star_border, size: 15.0),
                SizedBox(width: 5.0.w,),
                Text(stars.toString(),
                  style: TextStyle(
                      color: HexColor(HexColorCode.textColor),
                      fontSize: 12.0.sp,
                      fontWeight: FontWeight.w600
                  ),
                ),
              ],
            ),
            SizedBox(height: 12.0.h,),

          ],
        ),
      ),
    );
  }

}