import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:shimmer/shimmer.dart';

import '../../../../../core/constants/hex_color_code.dart';
import '../../../../../core/utility/color_tools.dart';

class RepositoryShimmerWidget extends StatelessWidget{
  final double height;
  final double width;
  const RepositoryShimmerWidget({super.key, required this.height, required this.width});

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: HexColor(HexColorCode.whiteColor),
      highlightColor: HexColor(HexColorCode.scaffoldBackgroundColor),
      child: Column(
        children: [
          SizedBox(height: 10.0.h,),
          RepositoryItemLoadShimmerWidget(height: height, width: width),
          SizedBox(height: 15.0.h,),
          RepositoryItemLoadShimmerWidget(height: height, width: width),
          SizedBox(height: 15.0.h,),
          RepositoryItemLoadShimmerWidget(height: height, width: width)
        ],
      ),
    );
  }

}

class RepositoryItemLoadShimmerWidget extends StatelessWidget{
  final double height;
  final double width;
  const RepositoryItemLoadShimmerWidget({super.key, required this.height, required this.width});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 16.0.w),
      width: width,
      height: height,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15.0),
        color: HexColor(HexColorCode.whiteColor),
      ),
    );
  }

}