import 'dart:io';

import 'package:path_provider/path_provider.dart';

import '../../../../core/constants/urls.dart';
import '../../../../core/network/api_service.dart';

class RepositoryDataSource {
  static Future<String> getRepositories(
      String query, int currentPage, String sort) async {
    //cache file
    String fileName = "repositoryCache.json";
    var dir = await getTemporaryDirectory();
    File file = File("${dir.path}/$fileName");
    var responseJson = "";
    try {
      final result = await InternetAddress.lookup('api.github.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        responseJson = await ApiService().getRequest(
            "${ApiCredential.searchRepositories}?q=$query&per_page=10&page=$currentPage&sort=$sort");
        file.writeAsStringSync(responseJson, flush: true, mode: FileMode.write);
        return responseJson;
      }
    } on SocketException catch (_) {
      if (file.existsSync()) {
        final responseData = file.readAsStringSync();
        if (currentPage == 1) {
          return responseData;
        } else {
          return responseJson;
        }
      } else {
        return responseJson;
      }
    }
    return responseJson;
  }
}
