import 'dart:convert';

import 'package:dartz/dartz.dart';

import '../data_sources/repository_data_source.dart';
import '../models/repositories_model.dart';
import '../../domain/repositories/flutter_repository.dart';

class FlutterRepositoryImp extends FlutterRepository{

  @override
  Future<Either<Exception, RepositoriesModel>> fetchRepositories(String query, int currentPage, String sort) async{
    try {
      final responseJson = await RepositoryDataSource.getRepositories(query, currentPage, sort);
      final decodedJson = json.decode(responseJson);
      RepositoriesModel repositoriesModel = RepositoriesModel.fromJson(decodedJson);
      return Right(repositoriesModel);
    } catch (e) {
      return Left(Exception(e));
    }
  }

}