import 'repository_owner_model.dart';

class RepositoryDetailsModel{
  final int? id;
  final String? fullName;
  final String? description;
  final String? updatedAt;
  final int? stargazersCount;
  final RepositoryOwnerModel? owner;

  RepositoryDetailsModel({
    this.id,
    this.fullName,
    this.description,
    this.updatedAt,
    this.stargazersCount,
    this.owner
  });

  factory RepositoryDetailsModel.fromJson(Map<String, dynamic> json) => RepositoryDetailsModel(
    id: json["id"],
    fullName : json['full_name'],
    description : json['description'],
    updatedAt : json['updated_at'],
    stargazersCount : json['stargazers_count'],
    owner: json["owner"] == null ? null : RepositoryOwnerModel.fromJson(json["owner"]),
  );


  Map<String, dynamic> toJson() => {
    "id": id,
    "full_name": fullName,
    "description": description,
    "updated_at": updatedAt,
    "stargazers_count": stargazersCount,
    "owner": owner == null ? null : owner!.toJson(),
  };
}