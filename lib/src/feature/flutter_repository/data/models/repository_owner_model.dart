class RepositoryOwnerModel{
  final String? login;
  final String? avatarUrl;

  RepositoryOwnerModel({
    this.login,
    this.avatarUrl
  });

  factory RepositoryOwnerModel.fromJson(Map<String, dynamic> json) => RepositoryOwnerModel(
    login: json["login"],
    avatarUrl : json['avatar_url'],
  );


  Map<String, dynamic> toJson() => {
    "login": login,
    "avatar_url": avatarUrl,
  };

}