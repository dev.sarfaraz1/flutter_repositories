import 'repository_detail_model.dart';

class RepositoriesModel{
  final int? totalCount;
  final bool? incompleteResults;
  final List<RepositoryDetailsModel>? items;

  RepositoriesModel({
    this.totalCount,
    this.incompleteResults,
    this.items
  });

  factory RepositoriesModel.fromJson(Map<String, dynamic> json) => RepositoriesModel(
    totalCount: json["total_count"],
    incompleteResults : json['incomplete_results'],
    items : json['items'] == null ? null : List<RepositoryDetailsModel>.from(json['items'].map((x) => RepositoryDetailsModel.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "total_count": totalCount,
    "incomplete_results": incompleteResults,
    "items": items == null ? null : List<dynamic>.from(items!.map((x) => x.toJson()))
  };
}