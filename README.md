# Flutter Repositories

## Overview

This is a Flutter application that displays the GitHub repositories by searching with the keyword "Flutter". It showcases the implementation of a large scalable project with features such as fetching repositories from the GitHub API, offline mode support, pagination, sorting, repository details, and more. The project follows the principles of Clean Architecture and utilizes the Riverpod library for state management.

## Features

- Fetches repository list from the GitHub API using the keyword "Flutter".
- Implements pagination for fetching 10 new items by scrolling.
- Implements sorting by the last updated date-time or star count.
- Displays a list of repositories on the home page.
- Provides a repository details page showing information about a selected repository.
- Supports offline browsing by caching the loaded repository list and details data.

## Installation
To get started with the project, please follow these steps:
1. Clone the repository to your local machine.
2. Open the project in your preferred IDE (e.g., Android Studio, Visual Studio Code).
3. Run the following command `flutter pub get` to install the required dependencies.

## Usage
Build and run the project using the Flutter SDK and your preferred IDE or by running the `flutter run` command in the terminal.

## Screenshots

<table>
  <tr>
   <td>Repositories Screen</td>
   <td>Details Screen</td>
  </tr>
  <tr>
    <td width="250", height="250"><img src="https://gitlab.com/dev.sarfaraz1/flutter_repositories/uploads/103e9d5c6bf3276238a0ed35ed21f0a2/1.jpg"></td>
    <td width="250", height="250"><img src="https://gitlab.com/dev.sarfaraz1/flutter_repositories/uploads/f76234b072c515de8179e4b73dee523c/2.jpg"></td>
  </tr>
</table>

## Screenshow

<img src="https://gitlab.com/dev.sarfaraz1/flutter_repositories/uploads/cdd4130d4934492255b79ab035a8294d/repo-gif.gif" height="500">

## Dependencies

``` dart
  cupertino_icons: ^1.0.2
  flutter_screenutil: ^5.6.0
  flutter_riverpod: ^2.3.6
  get_it: ^7.6.0
  dio: ^5.2.1+1
  go_router: ^8.0.5
  cached_network_image: ^3.2.3
  shimmer: ^3.0.0
  readmore: ^2.2.0
  intl: ^0.18.1
  path_provider: ^2.0.15
  dartz: 0.10.1
```

## Project Structure & Architecture
```
|-- lib
    |-- src
        |-- core
            |-- constants
            |-- di
            |-- enums
            |-- network
            |-- routes
            |-- utility
        |-- feature
            |-- data
                |-- data_sources
                |-- models
                |-- repositories
            |-- domain
                |-- repositories
                |-- use_cases
            |-- presentation
                |-- provider
                |-- screens
                |-- services
                |-- widgets
    |-- main.dart
```
This structure separates the concerns and provides a clear separation of the core, data, domain, and presentation layers, following the principles of Feature based Clean Architecture. Each layer has its own responsibilities, making the codebase more maintainable, testable, and scalable.


## State Management

The application utilizes the Riverpod library for state management. It follows a provider-based approach to manage and access state throughout the application. Providers are defined using the `Provider` or `StateProvider` family of classes, and state changes can be observed using the `ref.watch` or `ref.read` methods provided by Riverpod.


## Build

- [APK Link](https://drive.google.com/file/d/1YmAgpQ3dRNDX9-DAnA6dLCZLHC4uEmJT/view?usp=sharing)


## Future Enhancements

The current version of the application provides a solid foundation with essential features. However, there are several areas that can be enhanced in the future, including:

- Implementing a retry mechanism for failed API requests.
- Enhancing the UI/UX design and adding more interactive features.
- Incorporating more test coverage for increased code reliability.

## Author

This application is developed by [Sarfaraz](https://sarfarazahmed008.github.io/).
